import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

//import bookmarkBackgroundImage from "%PUBLIC_URL%/assets/general/bookmark.png";

function Navbar( { activeSection } ) {

	let cssHomeClass = ' bookmark ';
	let cssCharacterClass = ' bookmark ';
	let cssJournalClass = ' bookmark ';
	let cssInventoryClass = ' bookmark ';
	let cssOptionsClass = ' bookmark ';

	switch ( activeSection )
	{
		case 'home':
			cssHomeClass += ' bookmark-active ';
		break;
		case 'character':
			cssCharacterClass += ' bookmark-active ';
		break;
		case 'journal':
			cssJournalClass += ' bookmark-active ';
		break;
		case 'inventory':
			cssInventoryClass += ' bookmark-active ';
		break;
		case 'options':
			cssOptionsClass += ' bookmark-active ';
		break;
		default:
		break;
	}

	return (
		<nav className='book-nav'>
			<Link to='/' className={`${cssHomeClass}`} >
				Home
			</Link>
			<Link to='/character' className={`${cssCharacterClass}`} >
				Character
			</Link>
			<Link to='/journal' className={`${cssJournalClass}`} >
				Journal
			</Link>
			<Link to='/inventory' className={`${cssInventoryClass}`} >
				Inventory
			</Link>
			<Link to='/options' className={`${cssOptionsClass}`} >
				Options
			</Link>
		</nav>
	)
}

export default Navbar
