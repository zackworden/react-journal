import React from 'react'
import BookPage_Quest from './BookPage_Quest';
import BookPage_QuestClues from './BookPage_QuestClues';

import '../../../css/quest.css';

function BookSection_Journal( { currentQuest, currentClues } ) {

	return (
		<section className='bookSection bookSection-journal'>
			<BookPage_Quest questTitle={ currentQuest.title } questDescription={ currentQuest.description } questImage={ currentQuest.image } />
			<BookPage_QuestClues questClues={ currentClues } currentClues={ currentClues } />
		</section>
	)
}

export default BookSection_Journal
