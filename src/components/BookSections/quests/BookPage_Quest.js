import React from 'react';

function BookPage_Quest( { questTitle, questDescription, questImage } ) {
	
	return (
		<div className='bookPage bookPage-quest'>
			{
				questImage &&
				<img src={ questImage } />
			}
			{ questTitle && 
				<h3 className='questTitle'>
					Quest: { questTitle }
				</h3>				
			}
			{
				questDescription &&
				<p>
					{ questDescription }
				</p>
			}
		</div>
	)
}

export default BookPage_Quest
