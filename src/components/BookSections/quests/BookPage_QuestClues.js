import React from 'react';

function BookPage_QuestClues( { questClues, currentClues } ) {
	
	return (
		<div className='bookPage bookPage-questClues'>
			{
				currentClues.map( ( thisClue ) =>
					{
						return (
							<div className='questClue questClue-real' key={ thisClue.id }>
								<div>{ thisClue.clue }</div>
								<cite>-- { thisClue.source }</cite>
							</div>
						);
					}
				)
			} 
			{
				questClues.length == 0 &&
				(
					<div className='questClue questClue-none'>No clues yet...</div>
				)
			}
		</div>
	)
}

export default BookPage_QuestClues
