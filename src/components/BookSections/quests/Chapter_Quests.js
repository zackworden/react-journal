import React from 'react'

import BookSection_Journal from './BookSection_Journal';
import Navbar from '../../Navbar';
import SectionQuestNavbar from '../../Navigation/SectionQuestNavbar';

function Chapter_Quests( { clueData, currentQuestOffset, currentQuest, canIncrementQuest, canDecrementQuest, doIncrementQuest, doDecrementQuest, currentClues } ) {
	return (
		<div>
			<Navbar activeSection='journal' />
			<SectionQuestNavbar 
				pageOffset={ currentQuestOffset } 
				navSection='home'
				doIncrementPage={ doIncrementQuest } 
				doDecrementPage={ doDecrementQuest } 
				canIncrementPage={ canIncrementQuest } 
				canDecrementPage={ canDecrementQuest } 
			/>
			<div className='book-content'  >
				<BookSection_Journal 
					currentQuest={ currentQuest }
					clueData={ clueData } 
					currentQuestOffset={ currentQuestOffset } 
					currentClues={ currentClues } 
				/>
			</div>
			
		</div>

	)
}

export default Chapter_Quests
