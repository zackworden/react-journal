import React from 'react'
import InventorySlot from './inventorySlot';


function BookPage_InventoryChar( { allSlots, unequipInventorySlot, allItems } ) {
	return (
		<div className='bookPage bookPage-charInventory'>
			{
				allSlots.map( ( thisSlot ) => { 
					let matchedItem =  allItems.filter( ( thisItem ) => thisItem.id == thisSlot.currentItem );
					let matchedItemImage = '';

					if ( matchedItem.length > 0 )
					{
						matchedItemImage = matchedItem[0].image;
					}
					
					return (
						<InventorySlot 
							key={ thisSlot.id } 
							slot={ thisSlot } 
							onUnequip={unequipInventorySlot } 
							itemImage={ matchedItemImage }
						/>
					)
				})
			}
		</div>
	)
}

export default BookPage_InventoryChar
