import React from 'react'

import InventorySlot from './inventorySlot';

import { useState } from 'react';

function BookPage_InventoryItems( { allItems, equipInventoryItem } ) {

	const allWeapons = allItems.filter( (thisItem) => thisItem.slotType == 'player_weapon' );

	const allArmorAndShields = allItems.filter( 
		(thisItem) => thisItem.slotType == 'player_shield' ||  
		thisItem.slotType == 'player_head' ||  
		thisItem.slotType == 'player_body' ||  
		thisItem.slotType == 'player_boots'
	);

	const allProfaneItems = allItems.filter( ( thisItem ) => thisItem.slotType == 'player_forbidden' );

	return (
		<div className='bookPage bookPage-items'>
			<h3 className='inventoryTitle'>
				Armor & Shields
			</h3>
			<div className='inventoryGroup'>
				{
					allArmorAndShields.map( ( thisItem ) => { 
						return (
							<button className='itemSlot' key={thisItem.id} onClick={ () => { equipInventoryItem( thisItem.id ) } } >{thisItem.name}</button>
						)
					})
				}
			</div>

			<h3 className='inventoryTitle'>
				Weapons
			</h3>
			<div className='inventoryGroup'>
				{
					allWeapons.map( ( thisItem ) => { 
						return (
							<button className='itemSlot' key={thisItem.id} onClick={ () => { equipInventoryItem( thisItem.id ) } } >{thisItem.name}</button>
						)
					})
				}
			</div>

			<h3 className='inventoryTitle'>
				Forbidden
			</h3>
			<div className='inventoryGroup'>
				{
					allProfaneItems.map( ( thisItem ) => { 
						return (
							<button className='itemSlot' key={thisItem.id} onClick={ () => { equipInventoryItem( thisItem.id ) } } >{thisItem.name}</button>
						)
					})
				}
			</div>

		</div>
	)
}

export default BookPage_InventoryItems
