import React from 'react'

import BookSection_Inventory from './BookSection_Inventory';
import Navbar from '../../Navbar';
import SectionNavbar from '../../Navigation/SectionNavbar';


function Chapter_Inventory( { allAvailableItems, inventorySlots, inventoryItems, equipInventoryItem, unequipInventorySlot } ) {
	return (
		<div>
			<Navbar activeSection='inventory' />
			<SectionNavbar />
			<div className='book-content'  >
				<BookSection_Inventory 
					allAvailableItems={ allAvailableItems } 
					inventorySlots={ inventorySlots } 
					inventoryItems={ inventoryItems } 
					equipInventoryItem={ equipInventoryItem } 
					unequipInventorySlot={ unequipInventorySlot } 
				/>
			</div>
			
		</div>
	)
}

export default Chapter_Inventory
