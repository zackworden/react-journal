import React from 'react';
import BookPage_InventoryItems from './BookPage_InventoryItems';
import BookPage_InventoryChar from './BookPage_InventoryChar';

import { useState } from 'react';

import '../../../css/inventory.css';

function BookSection_Inventory( { allAvailableItems, inventorySlots, inventoryItems, equipInventoryItem, unequipInventorySlot } ) {

	return (
		<section className='bookSection bookSection-inventory'>
			<BookPage_InventoryChar allSlots={ inventorySlots } unequipInventorySlot={ unequipInventorySlot } allItems={ allAvailableItems } />
			<BookPage_InventoryItems allItems={ inventoryItems } equipInventoryItem={ equipInventoryItem } />
		</section>
	)
}

export default BookSection_Inventory;
