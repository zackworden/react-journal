import React from 'react'



function InventorySlot( { slot, onUnequip, itemImage } ) {

	let slotLabel = 'Empty';
	
	if ( itemImage != '' )
	{
		slotLabel = '';
	}

	return (
		<button className={`inventorySlot inventorySlot-${ slot.id }`} key={ slot.id } onClick={ ( e ) => { onUnequip( slot.id); e.target.blur(); } } style={{ backgroundImage: `url( ${ itemImage } )`, backgroundSize: '100%' }} >
			{slotLabel}
		</button>
	)
}



export default InventorySlot
