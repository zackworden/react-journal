import React from 'react'

function BookPage( props ) {

	return (
		<section className='bookPage bookPage-generic'>
			{ props.children }
		</section>
	)
}

export default BookPage
