import React from 'react'
import BookPage from './BookPage';


function BookSection( props ) {
	
	let childPages = React.Children.toArray( props.children );
	let pageOffset = props.pageOffset;
	let pageElem = childPages[ pageOffset ];
	let nextPageElem = '';
	let nextPageOffset = 0;

	if ( props.children.length >= pageOffset + 1 )
	{
		nextPageOffset = pageOffset + 1;
		nextPageElem = childPages[ nextPageOffset ];
	}

	// if there is an odd number of pages, have an 'empty' page on the right
	if ( 
		props.children.length % 2 != 0 &&
		pageOffset == props.children.length - 1
	)
	{
		nextPageElem = ( <BookPage >empty</BookPage> );
	}

	// share the number of pages with app.js
	let numOfPageSetter = props.numOfPageSetter;
	numOfPageSetter( props.children.length );

	return (
		<section className='bookSection bookSection-generic'>
			{ pageElem }
			{ nextPageElem }
		</section>
	)
}

export default BookSection
