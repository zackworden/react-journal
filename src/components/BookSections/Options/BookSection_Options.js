import React from 'react'
import BookPage_Options from './BookPage_Options';
import '../../../css/options.css';

function BookSection_Options( { doChangeCharName, doRandomizeQuests, doRandomizedClues, doRandomizeCharacter, doRandomizeInventory, charData } ) {

	return (
		<section className='bookSection bookSection-character'>
			<BookPage_Options 
				doChangeCharName={ doChangeCharName } 
				doRandomizeQuests={ doRandomizeQuests } 
				doRandomizedClues={ doRandomizedClues } 
				doRandomizeCharacter={ doRandomizeCharacter } 
				doRandomizeInventory={ doRandomizeInventory } 
				charData={ charData }  
			/>
			{/* <BookPage_Options /> */}
		</section>
	)
}

export default BookSection_Options;
