import React from 'react'

import BookSection_Options from './BookSection_Options';
import Navbar from '../../Navbar';
import SectionNavbar from '../../Navigation/SectionNavbar';


function Chapter_Options( { doChangeCharName, doRandomizeQuests, doRandomizedClues, doRandomizeCharacter, doRandomizeInventory, charData } ) {
	return (
		<div>
			<Navbar activeSection='options' />
			<SectionNavbar />
			<div className='book-content'  >
				<BookSection_Options 
					doChangeCharName={ doChangeCharName }
					doRandomizeQuests={ doRandomizeQuests } 
					doRandomizedClues={ doRandomizedClues } 
					doRandomizeCharacter={ doRandomizeCharacter } 
					doRandomizeInventory={ doRandomizeInventory } 
					charData={ charData } 
				/>
			</div>
			
		</div>
	)
}

export default Chapter_Options
