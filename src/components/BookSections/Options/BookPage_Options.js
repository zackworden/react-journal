import React from 'react';
import GenericButton from '../../widgets/GenericButton';

function BookPage_Options( { doRandomizeQuests, doRandomizedClues, doRandomizeCharacter, doRandomizeInventory, charData, doChangeCharName } ) {

	let charName = charData.charName;
	return (
		<section className='bookPage bookPage-options'>
			<h3>
				Options
			</h3>
			<GenericButton
				buttonText='Get New Character!'
				buttonAction={ doRandomizeCharacter }
				buttonClassNames='13 456'
			/>
			<GenericButton
				buttonText='Get New Quests!'
				buttonAction={ doRandomizeQuests }
			/>
			<GenericButton
				buttonText='Keep Quest, Get New Clues!'
				buttonAction={ doRandomizedClues }
			/>
			<GenericButton
				buttonText='Randomize Inventory!'
				buttonAction={ doRandomizeInventory }
			/>
			New Character Name: <input type="text" defaultValue={charName} onChange={ doChangeCharName } />
		</section>
	)
}

export default BookPage_Options
