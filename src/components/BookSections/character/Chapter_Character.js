import React from 'react'

import BookCharacterSection from './BookSection_Character';
import Navbar from '../../Navbar';
import SectionNavbar from '../../Navigation/SectionNavbar';

function Chapter_Character( { charData, toolTipHandler } ) {
	return (
		<div>
			<Navbar activeSection='character' />
			<SectionNavbar />
			<div className='book-content'  >
				<BookCharacterSection 
					charData={ charData } 
					toolTipHandler={ toolTipHandler } 
				/>
			</div>
			
		</div>
	)
}

export default Chapter_Character
