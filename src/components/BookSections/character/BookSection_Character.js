import React from 'react'
import BookPage_Profile from './BookPage_Profile';
import BookPage_Stats from './BookPage_Stats';
import '../../../css/character.css';

function BookCharacterSection( { charData, toolTipHandler } ) {
	return (
		<section className='bookSection bookSection-character'>
			<BookPage_Profile charData={ charData } toolTipHandler={ toolTipHandler } />
			<BookPage_Stats charData={ charData } toolTipHandler={ toolTipHandler } />
		</section>
	)
}

export default BookCharacterSection
