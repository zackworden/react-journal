import React from 'react';
import CharacterStat from './CharacterStat';

function BookPage_Stats( { charData, toolTipHandler } ) {
	return (
		<div className='bookPage bookPage-charStats'>
			<h3>
				Stats
			</h3>
			<CharacterStat 
				statFamily='stats'
				statKey='strength'
				statLabel='Strength'
				statValue={ charData.strength }
				toolTipHandler={ toolTipHandler }
			/>
			<CharacterStat 
				statFamily='stats'
				statKey='dexterity'
				statLabel='Dexterity'
				statValue={ charData.dexterity }
				toolTipHandler={ toolTipHandler }
			/>
			<CharacterStat 
				statFamily='stats'
				statKey='intelligence'
				statLabel='Intelligence'
				statValue={ charData.intelligence }
				toolTipHandler={ toolTipHandler }
			/>
			<CharacterStat 
				statFamily='stats'
				statKey='wisdom'
				statLabel='Wisdom'
				statValue={ charData.wisdom }
				toolTipHandler={ toolTipHandler }
			/>
			<CharacterStat 
				statFamily='stats'
				statKey='constitution'
				statLabel='Constitution'
				statValue={ charData.constitution }
				toolTipHandler={ toolTipHandler }
			/>
			<CharacterStat 
				statFamily='stats'
				statKey='charisma'
				statLabel='Charisma'
				statValue={ charData.charisma }
				toolTipHandler={ toolTipHandler }
			/>
			<CharacterStat 
				statFamily='stats'
				statKey='experience'
				statLabel='Experience'
				statValue={ charData.experienceCurrent }
				toolTipHandler={ toolTipHandler }
			/>
		</div>
	)
}

export default BookPage_Stats
