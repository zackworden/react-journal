import React from 'react';
import CharacterStat from './CharacterStat';

function BookPage_Profile( { charData, toolTipHandler } ) {

	// do formula calculations based on current character data
	let saveFort = Math.floor( (charData.constitution / 2) + (charData.strength / 4) + charData.charLevel / 2 ) + 2;
	let saveWill = Math.floor( (charData.wisdom / 2) + (charData.intelligence / 8) + charData.charLevel / 2 ) + 2;
	let saveReflex = Math.floor( (charData.dexterity / 2) + (charData.constitution / 8) + (charData.strength / 8) + charData.charLevel / 2 ) + 2;

	return (
		<div className='bookPage bookPage-profile'>
			<div className='pageSection-headshot'>
				<img src={ charData.charAvatarImage } />
				<div>
					<h2 className='characterName'>
						{ charData.charName }
					</h2>
					<div className='characterTitle'>
						{ charData.charTitle }
					</div>
					<div className='characterClassRace'>
						{ charData.charRace } { charData.charClass } ( Level { charData.charLevel } )
					</div>
				</div>
			</div>
			<div className='pageSection-vitals'>
				<h3>
					Vitals
				</h3>
				<div className='ratioStat actionable' onClick={ () => { toolTipHandler('vitals', 'health') } }>
					<label>Health</label>
					<span>{ charData.healthCurrent } / { charData.healthMax }</span>
				</div>
				<div className='ratioStat actionable' onClick={ () => { toolTipHandler('vitals', 'mana') } }>
					<label>Mana</label>
					<span>{ charData.manaCurrent } / { charData.manaMax }</span>
				</div>
				<h3>
					<label>Saving Throws</label>
				</h3>
				<CharacterStat 
					statFamily='saves'
					statKey='fortitude'
					statLabel='Fortitude'
					statValue={ saveFort }
					toolTipHandler={ toolTipHandler }
				/>
				<CharacterStat 
					statFamily='saves'
					statKey='will'
					statLabel='Will'
					statValue={ saveWill }
					toolTipHandler={ toolTipHandler }
				/>
				<CharacterStat 
					statFamily='saves'
					statKey='reflex'
					statLabel='Reflex'
					statValue={ saveReflex }
					toolTipHandler={ toolTipHandler }
				/>
			</div>
		</div>
	)
}

export default BookPage_Profile
