import React from 'react'

function CharacterStat( { statFamily, statKey, statLabel, statValue, toolTipHandler } ) {

	return (
		<div tabIndex='0' className='numericStat actionable' onClick={ () => { toolTipHandler( statFamily, statKey) } } onKeyDown={ ( e ) => { if (e.keyCode == 13) toolTipHandler( statFamily, statKey)  } } >
			<label className='statLabel'>{ statLabel }</label><span className='statValue'>{ statValue }</span>
		</div>
	)
}

export default CharacterStat
