import React from 'react';

function SectionQuestNavbar( { pageOffset, doIncrementPage, doDecrementPage } )
{
	// todo
	let numOfPages = 4;


	const get_canDoPrevButton = ( pageOffset, e ) => {

		if ( pageOffset - 1 >= 0 )
		{
			return true;
		}
		else
		{
			return false;
		}
	};

	const get_canDoNextButton = ( pageOffset, numOfPages, e ) => {

		if ( pageOffset + 2 < numOfPages  )
		{
			return true;
		}
		else
		{
			return false;
		}
		
	};

	let btnLeft = '';
	let btnRight = '';

	if ( get_canDoPrevButton( pageOffset ) )
	{
		btnLeft = ( <button  className='dogearNav_prevBtn' onClick={ ( e ) => { doDecrementPage() } } >&larr; Previous</button> );
	}
	
	if ( get_canDoNextButton( pageOffset, numOfPages ) )
	{
		
		btnRight = ( <button  className='dogearNav_nextBtn' onClick={ ( e ) => { doIncrementPage(); } } >Next &rarr;</button> );	
	}


	return (
		<nav className='dogearNav'>
			<span className='dogearNav_left'>
				{btnLeft}
			</span>
			<span className='dogearNav_right' >
				{btnRight}
			</span>
		</nav>
	)
}

export default SectionQuestNavbar
