import React from 'react';

function SectionNavbar( { pageOffset, doIncrementPage, doDecrementPage, getCanDecrementPage, getCanIncrementPage, numOfPageGetter } )
{
	let numOfPages = -1;
	let btnLeft = '';
	let btnRight = '';

	if ( numOfPageGetter )
	{
		numOfPages = numOfPageGetter();
	}

	if ( getCanDecrementPage )
	{
		if ( getCanDecrementPage( pageOffset, numOfPages ) )
		{
			btnLeft = ( <button  className='dogearNav_prevBtn' onClick={ () => { doDecrementPage() } } >&larr; Previous</button> );
		}
	}
	
	if ( getCanIncrementPage )
	{
		if ( getCanIncrementPage( pageOffset, numOfPages ) )
		{
			btnRight = ( <button  className='dogearNav_nextBtn' onClick={ () => { doIncrementPage() } } >Next &rarr;</button> );	
		}
	}

	return (
		<nav className='dogearNav'>
			<span className='dogearNav_left'>
				{btnLeft}
			</span>
			<span className='dogearNav_right' >
				{btnRight}
			</span>
		</nav>
	)
}

export default SectionNavbar
