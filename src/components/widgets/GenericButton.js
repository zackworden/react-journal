import React from 'react'

function GenericButton( { buttonText, buttonAction, buttonClassNames } ) {
	return (
		<button className={ ` genericButton ${ buttonClassNames } ` } onClick={ () => { buttonAction() } } >
			{ buttonText }
		</button>
	)
}

GenericButton.defaultProps = {
	buttonText : 'A button',
	buttonAction : () => {},
	buttonClassNames : '',
};

export default GenericButton
