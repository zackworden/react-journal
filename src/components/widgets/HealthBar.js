import React from 'react'

//import {Progress} from 'uikit-react';

function HealthBar( { primaryColor, secondaryColor, maxNumber, smallNumber } ) {
	
	let healthPercentage = maxNumber / smallNumber;

	return (
		<div>
			<label></label>
			<progress value={smallNumber} max={maxNumber} style={{backgroundColor : 'red', height: '1rem', width: '100%' }} >
				<span>{smallNumber} / {maxNumber}</span>
			</progress>
		</div>
	);
		// <div className='healthBarWrapper'>
		// 	<span>{smallNumber} / {maxNumber}</span>
		// 	<div className='healthBar' style={{ backgroundColor: secondaryColor }} >
		// 		<div className='healthBar_background' style={{ backgroundColor: primaryColor, width: healthPercentage + '%' }} >
		// 		</div>
		// 	</div>
		// </div>
		
		// <Progress value='10' max='100' />
}

export default HealthBar;
