import React from 'react'

function Modal( { modalTitle, modalCopy } ) {
	return (
		<div id="the_modal" uk-modal>
			<div class="uk-modal-dialog uk-modal-body">
				<h2 class="uk-modal-title">{modalTitle}</h2>
				<p>{modalCopy}</p>
				<button class="uk-modal-close" type="button"></button>
			</div>
		</div>
	)
}

export default Modal
