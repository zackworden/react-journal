/*

since this is hosted on a static site & has no server to bounce requests off of right now, the fetch request is encapsulated here but faked

*/
import React from 'react'

function UseFetch() {

	const allCharClasses = [
		'Paladin',
		'Fighter',
		'Mystic',
		'Archaeologist',
		'Shaman',
		'Professor',
		'Oracle',
		'Lancer',
		'Numerologist',
		'Hotline Psychic',
	];

	const allCharTitles = [
		'Avenger of Destiny',
		'Purveyor of the Forbidden',
		'Slayer of Sorcerors',
		'the Profane',
		'Keeper of the Chalice',
		'le francais',
	];

	const allCharRaces = [
		'Human',
		'Deep-folk',
		'Fish Person',
		'Fungoid',
		'Crab Person',
		'Rockman',
		'Cocker Spaniel',
	];

	const allCharAligns = [
		'Lawful Good',
		'Lawful Neutral',
		'Lawful Evil',
		'Neutral Good',
		'True Neutral',
		'Neutral Evil',
		'Chaotic Good',
		'Chaotic Neutral',
		'Chaotic Evil',
		'Ascended',
		'Null & Void',
	];

	const allCharPortraits = [
		'./assets/portraits/portrait1.jpg',
		'./assets/portraits/portrait2.jpg',
		'./assets/portraits/portrait3.jpg',
		'./assets/portraits/portrait4.jpg',
		'./assets/portraits/portrait5.jpg',
		'./assets/portraits/portrait6.jpg',
		'./assets/portraits/portrait7.jpg',
		'./assets/portraits/portrait8.jpg',
	];

	const allQuests = [
		{
			id : 1,
			title : 'Find the Necronomicon!',
			description : 'The book of forbidden knowledge is close, but WHERE is it?! You must find it before the people of the town ask too many questions -- or worse, use the book themselves.',
			slug : 'necronomicon',
			image : './assets/quests/necronomicon.jpg',
		},
		{
			id : 2,
			title : 'What Dwells Beneath',
			description : 'Everyone knew the people of Innsmouth smelled terrible, but there is definitely something is fishy about this quant coastal New England town.',
			slug : 'fishpeople',
			image : './assets/quests/innsmouth.jpg',
		},
		{
			id : 3,
			title : 'Finish the Tutorial Level',
			description : 'You\'re adventures have begun! Finish the tutorial level, learn all about this fancy journal menu, and find the secrets of FORBIDDEN KNOWLEDGE today.',
			slug : 'tutorial',
			image : './assets/quests/tutorial.jpg',
		},
		{
			id : 4,
			title : 'Hunt Through The Dark',
			description : 'Description goes here',
			slug : 'huntthroughdark',
			image : './assets/quests/huntthroughdark.jpg',
		},
		{
			id : 5,
			title : 'The Spirits of the Field',
			description : 'Well-rooted trees overshadow the water. One may lie hear and see, each night, lights across the grass &ndash; but those that sleep here never awaken.',
			slug : 'fieldspirits',
			image : '',
		},
		{
			id : 6,
			title : 'Stranded on Ice!',
			description : 'The ship has been caught in the ice! With the ship being slowly broken apart, it is only a matter of time until the expedition freezes to death. Unless...',
			slug : 'strandedonice',
			image : './assets/quests/stranded.jpg',
		},
	];

	const allClues = [
		{
			id : 1,
			source : 'Old Man Elbert',
			clue : 'You gotta watch out for the chickens up that road. Some of them be watchin us all a little too smartly, if ya ask me.',
			quests: ['necronomicon'],
		},
		{
			id : 2,
			source : 'Pauline the Shopkeep',
			clue : 'Careful around those woods. The townsfolk know to stay away, sure, but the flat-landers come on up, enter those woods, and disappear \'for you know it.',
			quests: ['fishpeople'],
		},
		{
			id : 3,
			source : 'Officer Dunner',
			clue : 'I don\'t care how many times I say it, I\'ll say it again. Stay off the grass. It holds terrible secrets.',
			quests: ['fishpeople'],
		},
		{
			id : 4,
			source : 'Edmund',
			clue : 'Books can be made of all sorts of materials. Paper, leather, clay - I once heard of a book made of a mummy\'s skin. Terrible book. But intriguing.',
			quests: ['necronomicon'],
		},
		{
			id : 5,
			source : 'Reese the dog',
			clue : 'Make sure to check out the `options` section &ndash; you can randomize your character, get new quests, and play with all kinds of stateful things!',
			quests: ['tutorial'],
		},
		{
			id : 6,
			source : 'Luna the dog',
			clue : 'Some say the forbidden idol is forbidden. `Fools` I call them!',
			quests: ['tutorial'],
		},
		{
			id : 7,
			source : 'Roxbury Pond the dog',
			clue : 'Curious about what this is? Check out the `home` section!',
			quests: ['tutorial'],
		},
		{
			id : 8,
			source : 'Bob, son of Rob',
			clue : 'Ever look out into the field late at night? The glowing lights dance across the grass. I wouldn\'t actually touch them, of course.',
			quests: ['fieldspirits'],
		},
		{
			id : 9,
			source : 'Rob, father of Bob',
			clue : 'I was out back just yesterday morning. Weird marks all over the grass. Like it been burned or something.',
			quests: ['fieldspirits'],
		},
		{
			id : 10,
			source : 'Professor Flint',
			clue : 'This whole area used to hold ancient but profane beliefs. Some credible sources even indicate that they sold their very humanity for some ancient and unspeakable evil.',
			quests: ['fishpeople', 'necronomicon'],
		},
		{
			id : 11,
			source : 'Dorian',
			clue : 'Fish people? Here? Hah! If you want fish people, you need to go downeast over to Innsmouth.',
			quests: ['fishpeople'],
		},
		{
			id : 12,
			source : 'Doctor Brant',
			clue : 'Fish... people? Human beings and fish, melded in some sort of unholy ritual? That sounds scientifically plausible to me.',
			quests: ['fishpeople'],
		},
		{
			id : 13,
			source : 'A. Alhazred',
			clue : 'Why yes, I\'ve written many books. Some books... more decipherable than others. But you must to understand; some truths are more decipherable than others, and I merely inked the pages.',
			quests: ['necronomicon'],
		},
		{
			id : 14,
			source : 'Duo, la chouette tetue',
			clue : 'I see you haven\'t studied your forbidden knowledge for today! Why not open the book for five minutes and start on a new FORBIDDEN LESSON now?',
			quests: ['necronomicon'],
		},
		{
			id : 15,
			source : 'Sir R. Priestly',
			clue : 'For scientific discovery give me Scott. For speed and efficiency of travel give me Amundsen. But when disaster strikes and all hope is gone, get down on your knees and pray for Shackleton.',
			quests: ['strandedonice'],
		},
	];

	const allAvailableItems = [
		{
			id : 1,
			name : 'Leather Armor',
			slotType : 'player_body',
			image : `./assets/inventory/armor/leather.png`,
		},
		{
			id : 2,
			name : 'Chain Armor',
			slotType : 'player_body',
			image : `./assets/inventory/armor/chainmail.png`,
		},
		{
			id : 3,
			name : 'Plate Armor',
			slotType : 'player_body',
			image : `./assets/inventory/armor/plateReal.png`,
		},
		{
			id : 4,
			name : 'Buckler',
			slotType : 'player_shield',
			image : `./assets/inventory/armor/round_shield.png`,
		},
		{
			id : 5,
			name : 'Kite Shield',
			slotType : 'player_shield',
			image : `./assets/inventory/armor/kite_shield.png`,
		},
		{
			id : 6,
			name : 'Sword',
			slotType : 'player_weapon',
			image : `./assets/inventory/weapons/sword.png`,
		},
		{
			id : 7,
			name : 'Mace',
			slotType : 'player_weapon',
			image : `./assets/inventory/weapons/mace.png`,
		},
		{
			id : 8,
			name : 'Axe',
			slotType : 'player_weapon',
			image : `./assets/inventory/weapons/axe.png`,
		},
		{
			id : 9,
			name : 'Mysterious Gun',
			slotType : 'player_weapon',
			image : `./assets/inventory/weapons/bfg.png`,
		},
		{
			id : 10,
			name : 'Forbidden Idol',
			slotType : 'player_forbidden',
			image : `./assets/inventory/forbidden/idol.png`,
		},
		{
			id : 11,
			name : 'Crossbow',
			slotType : 'player_weapon',
			image : `./assets/inventory/weapons/crossbow.png`,
		},
	];

	let fakeRequestObject = {
		charClasses : allCharClasses,
		charTitles : allCharTitles,
		charRaces : allCharRaces,
		charAligns : allCharAligns,
		charPortraits : allCharPortraits,
		everyQuests : allQuests,
		everyClues : allClues,
		allItems : allAvailableItems,
	};

	return fakeRequestObject;
}

export default UseFetch
