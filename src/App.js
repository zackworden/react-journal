/*
TODO

handle scrollable / overflow / extra information on pages / clues / etc
use context api for something
use useeffect for something
url param for something
make a decision on questClueMediator


code quality
- split the remove item / add item / remove slot / add slot into separate functions for INVENTORY
- clean up app.js
- general code clean pass
- add code comments

accessibility
- tabindex on actionable items

mobile friendly
- everything

polish
- actual book background
- visual consistency pass
- subtle animations as polish
- style evil version of the book
- preload images


*/
import Navbar from './components/Navbar';
import SectionNavbar from './components/Navigation/SectionNavbar';

import Chapter_Character from './components/BookSections/character/Chapter_Character';
import Chapter_Inventory from './components/BookSections/inventory/Chapter_Inventory';
import Chapter_Options from './components/BookSections/options/Chapter_Options';
import Chapter_Quests from './components/BookSections/quests/Chapter_Quests';


import BookPage from './components/BookSections/shared/BookPage';
import BookSection from './components/BookSections/shared/BookSection';

import './css/general.css';

import { createContext, useState, useEffect } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import UseFetch from './components/hooks/UseFetch';




function App(  ) {

	// fake rest request that returns all data from 'server'
	let fakeRequestObject = UseFetch();

	const allCharClasses = fakeRequestObject.charClasses;
	const allCharTitles = fakeRequestObject.charTitles;
	const allCharRaces = fakeRequestObject.charRaces;
	const allCharAligns = fakeRequestObject.charAligns;
	const allCharPortraits = fakeRequestObject.charPortraits;
	const allQuests = fakeRequestObject.everyQuests;
	const allClues = fakeRequestObject.everyClues;
	const allAvailableItems = fakeRequestObject.allItems;

	/*
	home functionality
	*/
		// home functionality container
		const homeFunctionality = {

			homePageOffset : -1,
			setHomePageOffset : '',
			
			numOfPages : -1,
			setNumOfPages : {},

			// get current num of pages for home section (since this can be dynamic)
			getNumOfPages : () => {
				return homeFunctionality.numOfPages;
			},

			// do decrement page action
			doDecrementPage : ( e ) => {
		
				if ( homeFunctionality.pageOffsetHome >= 1 )
				{
					homeFunctionality.setPageOffsetHome( homeFunctionality.pageOffsetHome - 2 );
				}
			},

			// do increment page action
			doIncrementPage : ( e ) => {
		
				homeFunctionality.setPageOffsetHome( homeFunctionality.pageOffsetHome + 2 );
			},	

			// can the current page be decremented?
			getCanDecrementPage : ( pageOffset, numOfPages, e ) => {

				if ( pageOffset - 2 >= 0 )
				{
					return true;
				}
				else
				{
					return false;
				}
			},

			// can the current page be incremented?
			getCanIncrementPage : ( pageOffset, numOfPages, e ) => {

				if ( pageOffset + 2 <= numOfPages  )
				{
					return true;
				}
				else
				{
					return false;
				}
				
			}

		};
		// set home states
		[ homeFunctionality.pageOffsetHome , homeFunctionality.setPageOffsetHome ] = useState( 0 );
		[ homeFunctionality.numOfPages, homeFunctionality.setNumOfPages ] = useState( -1 );



	/*
	character chapter functionality
	*/
		// character functionality container
		const characterFunctionality = {

			characterData : {},
			setCharacterData : {},

			getRandomizedCharacter : ( allCharClasses, allCharTitles, allCharRaces, allCharAligns, allCharPortraits, e ) => {
		
				let newCharName = 'Zack';
				let newCharClass = allCharClasses[ Math.floor( Math.random() * allCharClasses.length ) ];
				let newCharTitle = allCharTitles[ Math.floor( Math.random() * allCharTitles.length ) ];
				let newCharLevel = Math.floor( Math.random() * 4 ) + 1;
				let newCharRace = allCharRaces[ Math.floor( Math.random() * allCharRaces.length ) ];
				let newCharAlign = allCharAligns[ Math.floor( Math.random() * allCharAligns.length ) ];
				let newCharAvatarImage =  allCharPortraits[ Math.floor( Math.random() * allCharPortraits.length ) ];
				let newHealthMax = Math.floor( Math.random() * 100 ) + 25;
				let newHealthCurrent = 10;
				let newManaMax = Math.floor( Math.random() * 100 ) + 25;
				let newManaCurrent = 10;
				let newStrength = Math.floor( Math.random() * 5 ) + Math.floor( Math.random() * 5 ) + 1;
				let newDexterity = Math.floor( Math.random() * 5 ) + Math.floor( Math.random() * 5 ) + 1;
				let newIntelligence = Math.floor( Math.random() * 5 ) + Math.floor( Math.random() * 5 ) + 1;
				let newWisdom = Math.floor( Math.random() * 5 ) + Math.floor( Math.random() * 5 ) + 1;
				let newConstitution = Math.floor( Math.random() * 5 ) + Math.floor( Math.random() * 5 ) + 1;
				let newCharisma = Math.floor( Math.random() * 5 ) + Math.floor( Math.random() * 5 ) + 1;
				let newExperienceCurrent = 3400;
			
				return {
					charName : newCharName,
					charClass : newCharClass,
					charTitle : newCharTitle,
					charLevel : newCharLevel,
					charRace : newCharRace,
					charAlign : newCharAlign,
					charAvatarImage: newCharAvatarImage,
					healthCurrent : newHealthCurrent,
					healthMax : newHealthMax,
					manaCurrent : newManaCurrent,
					manaMax : newManaMax,
					strength : newStrength,
					dexterity : newDexterity,
					intelligence : newIntelligence,
					wisdom : newWisdom,
					constitution : newConstitution,
					charisma : newCharisma,
					experienceCurrent: newExperienceCurrent,
				};
			},

			doRandomizeCharacter : ( e ) => {

				let newChar = characterFunctionality.getRandomizedCharacter( allCharClasses, allCharTitles, allCharRaces, allCharAligns, allCharPortraits );
				characterFunctionality.setCharacterData( newChar );

				window.UIkit.modal.dialog( 'Random character created!' );
			},

			doChangeCharName : ( e ) => {

				let newCharData = characterFunctionality.characterData;

				if ( e.target.value != '' )
				{
					newCharData.charName = e.target.value;
				}
				else
				{
					newCharData.charName = 'the Nameless';
				}
		
				characterFunctionality.setCharacterData( newCharData );
			}

		};

		// set states
		let initialCharacter = characterFunctionality.getRandomizedCharacter( allCharClasses, allCharTitles, allCharRaces, allCharAligns, allCharPortraits );
		[ characterFunctionality.characterData, characterFunctionality.setCharacterData ] = useState( initialCharacter );


	/*
	all quest / clue chapter functionality
	*/

		// clue functionality
		const cluesFunctionality = {

			currentClues: {},
			setCurrentClues : {},
			clueData : {},
			setClueData : {},

			questClueMediator : {},

			getRandomizedClues : ( allClues, e ) => {

				let numOfCluesToGrab = 6;
				let cluesAlreadyPicked = [];
				let safetyCounter = 0;
			
				while ( numOfCluesToGrab > 0  && safetyCounter < 100 )
				{
					let possibleClue = allClues[ Math.floor( Math.random() * allClues.length ) ];
			
					if ( cluesAlreadyPicked.indexOf( possibleClue ) == -1 )
					{
						cluesAlreadyPicked.push( possibleClue );
						numOfCluesToGrab --;
					}
			
					safetyCounter ++;
				}
			
				return cluesAlreadyPicked;
			},

			doRandomizedClues : ( e ) => {

				let playerClues = cluesFunctionality.getRandomizedClues( allClues );
		
				cluesFunctionality.setClueData( playerClues );

				window.UIkit.modal.dialog( 'A new set of clues provided!' );
			},

			getCurrentClues : ( allClues, currentQuest) => {
			
				let currentClues = [];
		
				currentClues = allClues.filter (
					( thisClue ) => (thisClue.quests.indexOf( currentQuest.slug ) != -1)
				);
		
				return currentClues;
			}
			
		};

		// set states
		let initialClues = cluesFunctionality.getRandomizedClues( allClues );
		[ cluesFunctionality.clueData, cluesFunctionality.setClueData ] = useState( initialClues );
		[ cluesFunctionality.currentClues, cluesFunctionality.setCurrentClues ] = useState( [] );
	

		// quest functionality
		const questsFunctionality = {

			questData : {},
			setQuestData : {},

			questPageOffset : -1,
			setCurrentQuestPageOffset : {},

			clueHandler : {},

			questClueMediator : {},

			doRandomizedQuests : ( e ) => {

				let playerQuests = questsFunctionality.getRandomizedQuests( allQuests );
		
				questsFunctionality.setQuestData( playerQuests );

				window.UIkit.modal.dialog( 'New quests assigned!' );
			},

			getRandomizedQuests : ( allQuests, e ) => {

				let numOfQuestsToGrab = 3;
				let questsAlreadyPicked = [];
			
				while ( numOfQuestsToGrab > 0 )
				{
					let possibleQuest = allQuests[ Math.floor( Math.random() * allQuests.length ) ];
			
					if ( questsAlreadyPicked.indexOf( possibleQuest ) == -1 )
					{
						questsAlreadyPicked.push( possibleQuest );
						numOfQuestsToGrab --;
					}
				}
			
				return questsAlreadyPicked;
			},

			doIncrementQuest : ( e ) => {

				if ( 
					questsFunctionality.questData.length >= questsFunctionality.questPageOffset + 1 &&
					questsFunctionality.questData[ questsFunctionality.questPageOffset + 1 ]
				)
				{
					let newQuestOffset = questsFunctionality.questPageOffset + 1;
		
					questsFunctionality.setCurrentQuestPageOffset( newQuestOffset );
		
					questsFunctionality.clueHandler.setCurrentClues(
						questsFunctionality.clueHandler.clueData.filter( 
							( thisClue ) => thisClue.quests.indexOf( questsFunctionality.questData[ newQuestOffset ].slug ) != -1
						)
					);
				}
				else
				{
					window.alert('cant increment any more!');
				}
			},
		
			doDecrementQuest : ( e ) => {
		
				if ( questsFunctionality.questPageOffset - 1 >= 0 )
				{
					let newQuestOffset = questsFunctionality.questPageOffset - 1;
		
					questsFunctionality.setCurrentQuestPageOffset( newQuestOffset );
		
					questsFunctionality.clueHandler.setCurrentClues(
						questsFunctionality.clueHandler.clueData.filter( 
							( thisClue ) => thisClue.quests.indexOf( questsFunctionality.questData[ newQuestOffset ].slug ) != -1
						)
					);
				}
				else
				{
					window.alert('cant decrement any more!');
				}
			},

			getCanIncrementQuest : ( e ) => {

				let canIncrement = false;
		
				if (
					questsFunctionality.questData.length >= questsFunctionality.questPageOffset + 1 &&
					questsFunctionality.questData[ questsFunctionality.questPageOffset + 1 ]
				)
				{
					canIncrement = true;
				}
		
				return canIncrement;
			},
		
			getCanDecrementQuest : ( e ) => {
		
				let canDecrement = false;
		
				if (
					questsFunctionality.questPageOffset - 1 >= 0
				)
				{
					canDecrement = true;
				}
		
				return canDecrement;
			}
		};

		// todo - needed? it is just a demo, but maybe have a mediator obj
		const questClueMediator = {

			questHandler : questsFunctionality,
			clueHandler : cluesFunctionality
		};

		// todo - needed? init the mediator
		questsFunctionality.questClueMediator = questClueMediator;
		cluesFunctionality.questClueMediator = questClueMediator;
		

		let initialQuests = questsFunctionality.getRandomizedQuests( allQuests );

		[ questsFunctionality.questData, questsFunctionality.setQuestData ] = useState( initialQuests );
		[ questsFunctionality.questPageOffset, questsFunctionality.setCurrentQuestPageOffset ] = useState( 0 );

		const currentQuestClues = cluesFunctionality.getCurrentClues( cluesFunctionality.clueData, questsFunctionality.questData[questsFunctionality.questPageOffset] );
		questsFunctionality.clueHandler = cluesFunctionality;


	/*
	inventory functionality
	*/
		const inventoryFunctionality = {

			idolIsActive : false,
			setIdolIsActive : {},

			inventoryItems : {},
			setInventoryItems : {},
			
			inventorySlots : {},
			setInventorySlots : {},

			doRandomizeInventory : ( e ) => {
				window.UIkit.modal.dialog( 'TODO! Nothing for you yet.' );
			},

			// given item ID, equip it in the matching slot type
			equipInventoryItem : ( id ) => {
				
				let swapOutItem = undefined;
				let lookupItem = allAvailableItems.find( ( thisItem ) => thisItem.id == id );
				let targetSlot = inventoryFunctionality.inventorySlots.find( (thisSlot) => thisSlot.slotType == lookupItem.slotType );
				let targetSlotLocation = inventoryFunctionality.inventorySlots.indexOf( targetSlot );
			
				// check if target inventory slot is full
				// if so, move that item to inventory item pool && unequip
				if ( targetSlot.currentItem != -1 )
				{
					swapOutItem = allAvailableItems.find( ( thisItem ) => thisItem.id == targetSlot.currentItem );
				}

				// add item to equipped inventory slots
				let newSlot = {};
				newSlot = Object.assign( newSlot, targetSlot );
				newSlot.currentItem = id;

				let newInventorySlots = inventoryFunctionality.inventorySlots.slice(0);
				newInventorySlots[ targetSlotLocation ] = newSlot;

				// if the new inventory item is the forbidden idol, add a class to the app wrapper element
				if ( lookupItem.id == 10 )
				{
					inventoryFunctionality.setIdolIsActive( true );
				}

				// remove item from inventory item pool
				let removedItem = inventoryFunctionality.inventoryItems.find( (thisItem) => thisItem.id == id ); 
				let removedItemIndex = inventoryFunctionality.inventoryItems.indexOf( removedItem );
				let newInventoryItems = inventoryFunctionality.inventoryItems.slice(0);
				newInventoryItems.splice( removedItemIndex, 1 );

				// re-add any replacement items
				if ( swapOutItem != undefined )
				{
					newInventoryItems.push( swapOutItem );
				}

				// alpha the array
				newInventoryItems.sort( ( a, b ) => ( a.name > b.name ) ? 1 : -1  );

				// set states
				inventoryFunctionality.setInventorySlots( newInventorySlots );
				inventoryFunctionality.setInventoryItems( newInventoryItems );
			},

			// given a slot ID, unequip any items in slot and return to item pool
			unequipInventorySlot : ( id, e ) => {

				let targetSlot = inventoryFunctionality.inventorySlots.find( (thisSlot) => thisSlot.id == id );

				// special exception for the forbidden idol
				if ( 
					id == 5 &&
					targetSlot &&
					targetSlot.currentItem != -1 
				)
				{
					let audio = new Audio("/assets/inventory/forbidden/laugh.mp3");
					audio.play();
					// ?
				}
				else if (
					targetSlot && 
					targetSlot.currentItem != -1 
				)
				{
					let swapOutItem = allAvailableItems.find( ( thisItem ) => thisItem.id == targetSlot.currentItem );

					let newInventorySlots = inventoryFunctionality.inventorySlots.slice(0);
					let newInventorySlotIndex = inventoryFunctionality.inventorySlots.indexOf( targetSlot );

					newInventorySlots[ newInventorySlotIndex ].currentItem = -1;

					let newInventoryItems = inventoryFunctionality.inventoryItems.slice(0);
					newInventoryItems.push( swapOutItem );

					newInventoryItems.sort( ( a, b ) => ( a.name > b.name ) ? 1 : -1  );
					
					inventoryFunctionality.setInventorySlots( newInventorySlots );
					inventoryFunctionality.setInventoryItems( newInventoryItems );
				}

			},


			removeItemFromItemPool : ( item ) => {
				
			},

			addItemToItemPool : () => {

			},

		};

		// set states
		// state, evil idol - initially FALSE
		[ inventoryFunctionality.idolIsActive, inventoryFunctionality.setIdolIsActive ] = useState( false );

		// state, inventory item pool
		[ inventoryFunctionality.inventoryItems, inventoryFunctionality.setInventoryItems ] = useState( allAvailableItems );

		// state, inventory slots
		[ inventoryFunctionality.inventorySlots, inventoryFunctionality.setInventorySlots ] = useState(
			[
				{
					id : 0,
					label : 'Head',
					slotType : 'player_head',
					currentItem : -1,
				},
				{
					id : 1,
					label : 'Body',
					slotType : 'player_body',
					currentItem : -1,
				},
				{
					id : 2,
					label : 'Boots',
					slotType : 'player_boots',
					currentItem : -1,
				},
				{
					id : 3,
					label : 'Left Hand',
					slotType : 'player_shield',
					currentItem : -1,
				},
				{
					id : 4,
					label : 'Right Hand',
					slotType : 'player_weapon',
					currentItem : -1,
				},
				{
					id : 5,
					label : '?',
					slotType : 'player_forbidden',
					currentItem : -1,
				},
			]
		);


	/*
	everything else
	*/
		// tool tip handler
		const toolTipHandler = {

			toolTipData : {
				stats : {
					strength : 'Strength is a measure of your physical strength.',
					dexterity : 'Dexterity is how fast your character can move.',
					intelligence : 'Intelligence is the amount of specialty knowledge your character has.',
					wisdom : 'Wisdom is the amount of general knowledge your character has.',
					constitution : 'Constitution determines how healthy and physically resistant your character is.',
					charisma : 'Charisma measures how capable your character is with interpersonal relations.',
					experience : 'Experience determines your character\'s level.'
				},
				vitals : {
					health : 'How much life force you have left.',
					mana : 'How connected to the astral plane you are.'
				},
				saves : {
					fortitude : 'How resistant your character is to physical attacks.',
					will : 'How resistant your character is to mental attacks.',
					reflex : 'How capable your character is of dodging attacks.',
				},
			},

			showToolTip : ( toolTipType, toolTipKey ) => {

				if ( toolTipHandler.toolTipData[ toolTipType ][ toolTipKey ] )
				{
					window.UIkit.modal.dialog( toolTipHandler.toolTipData[ toolTipType ][ toolTipKey ] );
				}
				else 
				{
					console.log( 'Stat description for ' + toolTipKey + 'not found!');
				}
				
			}

		};
	


	return (
		<div className="bookMenuApp" data-idolisactive={ inventoryFunctionality.idolIsActive }>
			<div className='book' style={{ backgroundSize: '100%', backgroundRepeat: 'no-repeat', minHeight: '900px' }}>
				<Router>
					<Route exact path='/'>
						<Navbar activeSection='home' />
						<SectionNavbar 
							pageOffset={homeFunctionality.pageOffsetHome} 
							navSection='home'
							doIncrementPage={ homeFunctionality.doIncrementPage } 
							doDecrementPage={ homeFunctionality.doDecrementPage } 
							getCanIncrementPage={ homeFunctionality.getCanIncrementPage } 
							getCanDecrementPage={ homeFunctionality.getCanDecrementPage } 
							numOfPageGetter={ homeFunctionality.getNumOfPages }
						/>
					</Route>
					<div className='book-content'  >
						<Route exact path='/'>
							<BookSection pageOffset={homeFunctionality.pageOffsetHome} numOfPageSetter={homeFunctionality.setNumOfPages} >
								<BookPage >
									<h3>
										Generic Greetings To you!
									</h3>
									<p>
										Welcome to my react / journal portfolio item.
									</p>
								</BookPage>
								<BookPage >
									<h3>
										Page 2 - More Copy
									</h3>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquam auctor sollicitudin. Phasellus gravida massa sit amet dolor feugiat viverra. Nullam eu tortor magna. Phasellus quis massa turpis. Fusce ullamcorper dignissim convallis. Mauris neque ex, pulvinar eu volutpat gravida, tincidunt convallis odio. Cras porttitor congue ipsum, porttitor rhoncus diam pretium sed. Donec sit amet finibus magna. In mi nulla, vulputate a ligula ut, hendrerit ultricies nunc. Quisque auctor lobortis bibendum. Nunc a sagittis odio. Morbi ut interdum eros, vitae condimentum nisi. Ut fringilla justo pharetra, congue lacus et, fringilla risus.
									</p>
									<p>
										Cras eu interdum tortor. Aliquam sed massa eu dui pretium cursus. Duis erat enim, dictum vitae porta laoreet, semper id orci. In volutpat scelerisque viverra. Donec eget posuere mi, vel rhoncus tellus. Nulla dapibus consectetur velit, vitae luctus augue fringilla eget. Pellentesque dignissim vehicula massa, ac vestibulum odio tristique sit amet. Integer elit leo, venenatis vitae nisi at, pretium rhoncus urna. Fusce mi enim, maximus nec felis ac, luctus commodo ante. Mauris vel neque erat. Etiam sed odio a lectus tempus molestie vitae et mauris. Phasellus hendrerit ex eget leo facilisis, at aliquam metus blandit. Morbi sit amet mauris a ipsum mollis tempus.
									</p>
									<p>
										Mauris egestas tortor lorem, eu feugiat nunc dictum et. Nullam luctus enim vitae fermentum porta. Aliquam erat volutpat. Aliquam commodo, enim at fringilla venenatis, turpis lectus lacinia velit, eu ultricies risus lacus eget mauris. Suspendisse fringilla tempus congue. Vivamus imperdiet, augue vel suscipit mollis, neque nulla dignissim tortor, a venenatis purus massa vel purus. Curabitur nec quam pretium, cursus eros quis, pellentesque lacus. Nunc non porttitor leo. Vestibulum eu leo ac lorem mattis porta. Fusce nibh felis, laoreet sed ipsum ac, luctus tristique ipsum. Nunc condimentum urna quis ultricies suscipit. 
									</p>
								</BookPage>
								<BookPage >
									<h3>
										Page Three
									</h3>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquam auctor sollicitudin. Phasellus gravida massa sit amet dolor feugiat viverra. Nullam eu tortor magna. Phasellus quis massa turpis. Fusce ullamcorper dignissim convallis. Mauris neque ex, pulvinar eu volutpat gravida, tincidunt convallis odio. Cras porttitor congue ipsum, porttitor rhoncus diam pretium sed. Donec sit amet finibus magna. In mi nulla, vulputate a ligula ut, hendrerit ultricies nunc. Quisque auctor lobortis bibendum. Nunc a sagittis odio. Morbi ut interdum eros, vitae condimentum nisi. Ut fringilla justo pharetra, congue lacus et, fringilla risus.
									</p>
								</BookPage>
								<BookPage >
									<h3>
										Page 4
									</h3>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquam auctor sollicitudin. Phasellus gravida massa sit amet dolor feugiat viverra. Nullam eu tortor magna. Phasellus quis massa turpis. Fusce ullamcorper dignissim convallis. Mauris neque ex, pulvinar eu volutpat gravida, tincidunt convallis odio. Cras porttitor congue ipsum, porttitor rhoncus diam pretium sed. Donec sit amet finibus magna. In mi nulla, vulputate a ligula ut, hendrerit ultricies nunc. Quisque auctor lobortis bibendum. Nunc a sagittis odio. Morbi ut interdum eros, vitae condimentum nisi. Ut fringilla justo pharetra, congue lacus et, fringilla risus.
									</p>
								</BookPage>
								<BookPage >
									<h3>
										Page 5
									</h3>
									<p>
										Cras eu interdum tortor. Aliquam sed massa eu dui pretium cursus. Duis erat enim, dictum vitae porta laoreet, semper id orci. In volutpat scelerisque viverra. Donec eget posuere mi, vel rhoncus tellus. Nulla dapibus consectetur velit, vitae luctus augue fringilla eget. Pellentesque dignissim vehicula massa, ac vestibulum odio tristique sit amet. Integer elit leo, venenatis vitae nisi at, pretium rhoncus urna. Fusce mi enim, maximus nec felis ac, luctus commodo ante. Mauris vel neque erat. Etiam sed odio a lectus tempus molestie vitae et mauris. Phasellus hendrerit ex eget leo facilisis, at aliquam metus blandit. Morbi sit amet mauris a ipsum mollis tempus.
									</p>
									<p>
										Mauris egestas tortor lorem, eu feugiat nunc dictum et. Nullam luctus enim vitae fermentum porta. Aliquam erat volutpat. Aliquam commodo, enim at fringilla venenatis, turpis lectus lacinia velit, eu ultricies risus lacus eget mauris. Suspendisse fringilla tempus congue. Vivamus imperdiet, augue vel suscipit mollis, neque nulla dignissim tortor, a venenatis purus massa vel purus. Curabitur nec quam pretium, cursus eros quis, pellentesque lacus. Nunc non porttitor leo. Vestibulum eu leo ac lorem mattis porta. Fusce nibh felis, laoreet sed ipsum ac, luctus tristique ipsum. Nunc condimentum urna quis ultricies suscipit. 
									</p>
									<p>
										Cras eu interdum tortor. Aliquam sed massa eu dui pretium cursus. Duis erat enim, dictum vitae porta laoreet, semper id orci. In volutpat scelerisque viverra. Donec eget posuere mi, vel rhoncus tellus. Nulla dapibus consectetur velit, vitae luctus augue fringilla eget. Pellentesque dignissim vehicula massa, ac vestibulum odio tristique sit amet. Integer elit leo, venenatis vitae nisi at, pretium rhoncus urna. Fusce mi enim, maximus nec felis ac, luctus commodo ante. Mauris vel neque erat. Etiam sed odio a lectus tempus molestie vitae et mauris. Phasellus hendrerit ex eget leo facilisis, at aliquam metus blandit. Morbi sit amet mauris a ipsum mollis tempus.
									</p>
									<p>
										Mauris egestas tortor lorem, eu feugiat nunc dictum et. Nullam luctus enim vitae fermentum porta. Aliquam erat volutpat. Aliquam commodo, enim at fringilla venenatis, turpis lectus lacinia velit, eu ultricies risus lacus eget mauris. Suspendisse fringilla tempus congue. Vivamus imperdiet, augue vel suscipit mollis, neque nulla dignissim tortor, a venenatis purus massa vel purus. Curabitur nec quam pretium, cursus eros quis, pellentesque lacus. Nunc non porttitor leo. Vestibulum eu leo ac lorem mattis porta. Fusce nibh felis, laoreet sed ipsum ac, luctus tristique ipsum. Nunc condimentum urna quis ultricies suscipit. 
									</p>
								</BookPage>
							</BookSection>
						</Route>
						<Route path='/character'>
							<Chapter_Character 
								charData={ characterFunctionality.characterData } 
								toolTipHandler={toolTipHandler.showToolTip} 
							/>
						</Route>
						<Route path='/journal'>
							<Chapter_Quests 
								clueData={ cluesFunctionality.clueData } 
								currentQuestOffset={questsFunctionality.questPageOffset} 
								currentQuest={ questsFunctionality.questData[questsFunctionality.questPageOffset] } 
								canIncrementQuest={ questsFunctionality.getCanIncrementQuest() } 
								canDecrementQuest={ questsFunctionality.getCanDecrementQuest() } 
								doIncrementQuest={ questsFunctionality.doIncrementQuest } 
								doDecrementQuest={ questsFunctionality.doDecrementQuest } 
								currentClues={currentQuestClues} 
							/>
						</Route>
						<Route path='/inventory'>
							<Chapter_Inventory 
								allAvailableItems={allAvailableItems} 
								inventorySlots={ inventoryFunctionality.inventorySlots } 
								inventoryItems={ inventoryFunctionality.inventoryItems } 
								equipInventoryItem={ inventoryFunctionality.equipInventoryItem } 
								unequipInventorySlot={ inventoryFunctionality.unequipInventorySlot } 
							/>
						</Route>
						<Route path='/options'>
							<Chapter_Options 
								doRandomizeQuests={ questsFunctionality.doRandomizedQuests } 
								doRandomizedClues={ cluesFunctionality.doRandomizedClues } 
								doRandomizeCharacter={characterFunctionality.doRandomizeCharacter} 
								doRandomizeInventory={inventoryFunctionality.doRandomizeInventory} 
								doChangeCharName={characterFunctionality.doChangeCharName}
								charData={ characterFunctionality.characterData } 
							/>
						</Route>
					</div>
				</Router>
			</div>
		</div>
	);
};

export default App;
